import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import CustomNavbar from "./components/obects/CustomNavbar";
import CustomSidebar from "./components/obects/CustomSidebar";

class App extends Component {
  render() {
    return (
      <div className="App">

        <div className="Navbar">
          <CustomNavbar />
        </div>

        <div className="Sidenavbar">
          <CustomSidebar />
        </div>

        <div className="Body">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
          </p>
        </div>
      </div>
    );
  }
}

export default App;
