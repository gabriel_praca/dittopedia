import React from 'react';
import { Collapse, 
            Navbar, 
            NavbarToggler,
            NavbarBrand,
            Nav, 
            NavItem,
            InputGroupText,
            Badge} from 'reactstrap';


import SearchBar from "./CustomSearchbar";

import { Icon } from 'react-icons-kit';
import { user } from 'react-icons-kit/fa/user'

class CustomNavbar extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
        <div>
            <Navbar style={navBarStyle} light expand="md">
                <NavbarBrand href="/">
                    {/* <img src="https://img.pokemondb.net/sprites/ruby-sapphire/shiny/ditto.png" alt="Ditto"/> */}
                    {/* <Icon size={16} icon={logo}/> */}
                    Dittopedia
                </NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        {/* <NavItem>
                            <NavLink href="/components/">Components</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                        </NavItem> */}
                        <NavItem>
                            <SearchBar />
                        </NavItem>
                        <NavItem>
                            <InputGroupText>
                                <Icon icon={user}/>
                                {/* <Badge color="danger" pill>.</Badge> */}
                            </InputGroupText>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>);
    }
}

 const navBarStyle = {
   background: 'darkorchid'
   
 };

 const logoStyle = {
    width: '32px',
    heigth: '32px'
 };

 export default CustomNavbar;