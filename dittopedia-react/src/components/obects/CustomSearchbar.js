import React from 'react';

import { Icon } from 'react-icons-kit';
import { search } from 'react-icons-kit/fa/search';

import '../css/css.css';

import {Form,
        Button,
        Input,
        InputGroup,
        InputGroupAddon,
        InputGroupText} from 'reactstrap';

class CustomSearchbar extends React.Component {
    render() {
      return (
        <div>
            <Form>
                <InputGroup>
                    <Input className="customSearchText" placeholder="Search..." type="text"/>
                    <InputGroupAddon addonType="append">
                        <InputGroupText>
                                <Icon icon={search}/>
                        </InputGroupText>
                    </InputGroupAddon>
                </InputGroup>
            </Form>
        </div>
      );
    }
}

export default CustomSearchbar;