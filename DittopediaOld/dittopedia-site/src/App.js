import React, { Component } from 'react';
import NavBar from './components/pages/NavBar';
import HomePage from './components/pages/HomePage';
//import logo from './logo.svg';
//import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <NavBar />
          <HomePage />
      </div>
    );
  }
}

export default App;
