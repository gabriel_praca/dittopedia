import React from 'react';

class HomePage extends React.Component {
    render() {
        return (
            <div>
                <h3>This is a HomePage</h3>
            </div>
        )
    }
}

// const HomePage = () => (
//     <div>
//         <h1>This is a home page</h1>
//     </div>
// );

export default HomePage;