import React from 'react';
import { 
    Dropdown, 
    DropdownToggle, 
    DropdownMenu, 
    DropdownItem,
    Form,
    FormGroup,
    Label,
    Input} from 'reactstrap';

import { FaUser } from 'react-icons/fa';

export default class DropdownLogin extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  render() {
    return (
      <Dropdown direction='none' isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle>
            <FaUser/>
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem disabled>
                <Form>
                    <FormGroup>
                        <Label 
                            className="loginTextInput"
                            for="exampleEmail">Email</Label>
                        <Input 
                            className="loginTextInput"
                            type="email" 
                            name="email" 
                            id="exampleEmail" 
                            placeholder="with a placeholder" />
                    </FormGroup>
                    <FormGroup>
                        <Label 
                            className="loginTextInput"
                            for="examplePassword">Password</Label>
                        <Input 
                            className="loginTextInput"
                            type="password" 
                            name="password" 
                            id="examplePassword" 
                            placeholder="password placeholder" />
                    </FormGroup>
                </Form>
            </DropdownItem>
          {/* <DropdownItem header>Header</DropdownItem>
          <DropdownItem disabled>Action</DropdownItem>
          <DropdownItem>Another Action</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>Another Action</DropdownItem> */}
        </DropdownMenu>
      </Dropdown>
    );
  }
}