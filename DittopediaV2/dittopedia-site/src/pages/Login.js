import React from 'react';

import {
    Container,
    Row,
    Col, 
    Form,
    FormGroup, 
    Label, 
    Input,
    Button,
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter,
    InputGroupText} from 'reactstrap';
    
import { FaUser, FaGoogle, FaFacebookF } from 'react-icons/fa';

import '../styles/Default.css';

export default class Login extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        modal: false,
        backdrop: true
      };
  
      this.toggle = this.toggle.bind(this);
    }
  
    toggle() {
      this.setState({
        modal: !this.state.modal
      });
    }

    render(){
        return (
            <div>
                <InputGroupText className="login-button" onClick={this.toggle}>{this.props.buttonLabel}
                    <FaUser />
                </InputGroupText>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Login</ModalHeader>
                    <ModalBody>
                        <Container className="Login">
                            <Form className="form">
                                <Col>
                                    <FormGroup className="test">
                                            <Button className="login-google-button"
                                                onClick={this.toggle}>
                                                <FaGoogle className="login-button-icon"/>
                                            </Button>{' '}
                                            <Button className="login-facebook-button"
                                                onClick={this.toggle}>
                                                <FaFacebookF className="login-button-icon"/>
                                            </Button>{' '}
                                    </FormGroup>
                                </Col>
                                <Col>
                                    {/* <DropdownItem divider>test</DropdownItem> */}
                                    <FormGroup row>
                                        <Label sm={2}>Email: </Label>
                                        <Col sm={10}>
                                            <Input
                                                type="email"
                                                name="email"
                                                id="emailText"
                                                placeholder="email@email.com"/>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={2} for="LoginPassword">Senha: </Label>
                                        <Col sm={10}>
                                            <Input
                                                type="password"
                                                name="password"
                                                id="passwordText"
                                                placeholder="********"
                                            />
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col>
                                    <span> Ainda não é cadastrado?</span>
                                    <a href="#"> Registrar </a>
                                </Col>
                            </Form>
                        </Container>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Login</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}