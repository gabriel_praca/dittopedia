import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import CustomNavbar from './components/CustomNavbar';
import Sidebar from './components/Sidebar';
import PokemonList from './scripts/PokemonList';



import { Container } from 'reactstrap';

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <Sidebar />  */}
        <CustomNavbar />
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Bruno é viado</h1>
        </header> */}
        {/* <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p> */}
        <Container className="mb-3 mt-3">
          <PokemonList />
        </Container>
      </div>
    );
  }
}

export default App;
