import React from 'react';
import PokemonCard from '../components/PokemonCard';
import { Row, Col, CardColumns} from 'reactstrap';

const pokemonUrl = 'https://pokeapi.co/api/v2/pokemon/';

class PokemonList extends React.Component{
    
    constructor(props) {
        super(props);
    
        this.state = {
          data: [],
          isLoading: false,
          error: null,
        };
    }

    componentDidMount() {        
        this.setState({ isLoading: true });

        fetch(pokemonUrl)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(response => response.results)
            .then(data => this.setState({ data: data, isLoading: false }))
        .catch(error => this.setState({ error, isLoading: false }));
    }
    render() {
        const { data, isLoading, error } = this.state;
        
        if (error) {
            return <p>{error.message}</p>;
        }
        if (isLoading) {
          return <p>Loading ...</p>;
        }
        let count = 1;
        return (
            <div>
                {createTable(data.map((d) => <PokemonCard id={count++} pokemonObj={d}/>))}
            </div>
        );
    }        
}

const createTable = (listItems) => {
    let table = [];

    for (let i = 0; i < listItems.length / 4; i++) {
      table.push(
        <Row>
            {
                getLine(i, listItems)
            }
        </Row>
      )
    }
    return table
}

const getLine = (line, listItems) => {
    let row = [];
    let initialIDFromRow = (line * 4);

    for(var i = 0; i < 4; i++){
        row.push(
            <Col className="mb-3">
                {
                    listItems[initialIDFromRow + i]
                }
            </Col>
        )
    }
    return row
}

export default PokemonList;