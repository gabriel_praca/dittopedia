
create database dittopedia_dev;

use dittopedia_dev;

-- Criar esquemas diferentes para coisas do jogo e coisas do site
-- ex: schema pokemon - items, pokemons, loclidades
-- ex2: schema site - user, configuração...

create table pokemon_species
(
	species_id int auto_increment not null,
	name varchar(50) not null,

	constraint PK_SpeciesID primary key(species_id)
);

create table pokemon_type
(
	pokemon_type_id int auto_increment not null,
	name varchar(50) not null,

	constraint PK_PokemonTypeID primary key(pokemon_type_id)
);

create table pokemon_ability
(
    ability_id int auto_increment not null,
    name varchar(100),

	constraint PK_PokemonAbilityID primary key(ability_id)
);

create table pokemon_ability_description
(
    ability_id int not null,
    language char(10) not null,
    description varchar(255),
    
	constraint PK_AbilityDescriptionID primary key(ability_id, language),
	constraint FK_PokemonAbility_PokemonAbilityDescription foreign key(ability_id) references pokemon_ability(ability_id)
);

create table pokemon_form
(
    form_id int auto_increment not null,
    form_type varchar(50) not null,
    is_mega_evolution boolean not null,
    form_item int,
    
	constraint PK_PokemonFormID primary key(form_id)
	-- constraint FK_PokemonItems_PokemonAbilityDescription foreign key(ability_id) references pokemon_ability(ability_id)
);

create table pokemon_stats
(
    stats_id int auto_increment not null,
    name varchar(50) not null,
    
	constraint PK_PokemonStatsID primary key(stats_id)
);

create table pokemon
(
	pokemon_id int auto_increment not null,
    previous_evolution_id int,
	national_number int,
	name varchar(50) not null,
	species_id int not null,
	height varchar(50) not null,
	weight varchar(50) not null,
    base_experience int not null,
    form_id int,
    -- game_indices table list of games where he is found
        -- "game_index": 1,
        --     "version": {
        --         "name": "white-2",
        --         "url": "https://pokeapi.co/api/v2/version/22/"
        --     }
    -- location_area_encounters , local where encountered
    -- "moves": [
            -- {
            --     "move": {
            --         "name": "razor-wind",
            --         "url": "https://pokeapi.co/api/v2/move/13/"
            --     },
            --     "version_group_details": [
            --         {
            --             "level_learned_at": 0,
            --             "move_learn_method": {
            --                 "name": "egg",
            --                 "url": "https://pokeapi.co/api/v2/move-learn-method/2/"
            --             },
            --             "version_group": {
            --                 "name": "crystal",
            --                 "url": "https://pokeapi.co/api/v2/version-group/4/"
            --             }
            --         },
            --      ]
            -- }, ...
        -- ]

	constraint PK_PokemonID primary key(pokemon_id),
	constraint FK_Pokemon_PokemonSpecies foreign key(species_id) references pokemon_species(species_id),
	constraint FK_Pokemon_PokemonForm foreign key(form_id) references pokemon_form(form_id),
	constraint FK_Pokemon_PreviousEvolution foreign key(previous_evolution_id) references pokemon(pokemon_id)
);
	
create table stats_of_pokemon
(
	pokemon_id int not null,
    stats_id int not null,
    base_stat int not null,
    effort int not null,
	
	constraint PK_StatsOfPokemon primary key(pokemon_id, stats_id),
	constraint FK_StatsOfPokemon_Pokemon foreign key(pokemon_id) references pokemon(pokemon_id),
	constraint FK_StatsOfPokemon_PokemonStats foreign key(stats_id) references pokemon_stats(stats_id)
);

create table abilities_of_pokemon
(
	pokemon_id int not null,
    ability_id int not null,
    is_hidden boolean not null,
	
	constraint PK_AbilitiesOfPokemon primary key(pokemon_id, ability_id),
	constraint FK_AbilitiesOfPokemon_Pokemon foreign key(pokemon_id) references pokemon(pokemon_id),
	constraint FK_AbilitiesOfPokemon_PokemonAbility foreign key(ability_id) references pokemon_ability(ability_id)
);

create table types_of_pokemon
(
	pokemon_id int not null,
	pokemon_type_id int not null,
    pokemon_order smallint not null,
	
	constraint PK_TypesOfPokemon primary key(pokemon_id, pokemon_type_id),
	constraint FK_TypesOfPokemon_Pokemon foreign key(pokemon_id) references pokemon(pokemon_id),
	constraint FK_TypesOfPokemon_pokemonType foreign key(pokemon_type_id) references pokemon_type(pokemon_type_id)
);

-- Inserts

-- pokemon_species
-- A
insert into pokemon_species (name) values ('Abundant');
insert into pokemon_species (name) values ('Acorn');
insert into pokemon_species (name) values ('Alpha');
insert into pokemon_species (name) values ('Angler');
insert into pokemon_species (name) values ('Ant Pit');
insert into pokemon_species (name) values ('Anteater');
insert into pokemon_species (name) values ('Antenna');
insert into pokemon_species (name) values ('Aqua Mouse');
insert into pokemon_species (name) values ('Aqua Rabbit');
insert into pokemon_species (name) values ('Arm Thrust');
insert into pokemon_species (name) values ('Armor');
insert into pokemon_species (name) values ('Armor Bird');
insert into pokemon_species (name) values ('Arrow Quill');
insert into pokemon_species (name) values ('Artificial');
insert into pokemon_species (name) values ('Astral Body');
insert into pokemon_species (name) values ('Atrocious');
insert into pokemon_species (name) values ('Attaching');
insert into pokemon_species (name) values ('Aura');
insert into pokemon_species (name) values ('Aurora');
insert into pokemon_species (name) values ('Automaton');
insert into pokemon_species (name) values ('Avianoid');
insert into pokemon_species (name) values ('Axe Jaw');

-- B
insert into pokemon_species (name) values ('Bagworm');
insert into pokemon_species (name) values ('Ball');
insert into pokemon_species (name) values ('Ball Roll');
insert into pokemon_species (name) values ('Ball Whale');
insert into pokemon_species (name) values ('Balloon');
insert into pokemon_species (name) values ('Barnacle');
insert into pokemon_species (name) values ('Barrier');
insert into pokemon_species (name) values ('Bash Buffalo');
insert into pokemon_species (name) values ('Bat');
insert into pokemon_species (name) values ('Battery');
insert into pokemon_species (name) values ('Beak');
insert into pokemon_species (name) values ('Beaver');
insert into pokemon_species (name) values ('Beckon');
insert into pokemon_species (name) values ('Bee Fly');
insert into pokemon_species (name) values ('Beehive');
insert into pokemon_species (name) values ('Bell');
insert into pokemon_species (name) values ('Big Boss');
insert into pokemon_species (name) values ('Big Eater');
insert into pokemon_species (name) values ('Big Horn');
insert into pokemon_species (name) values ('Big Jaw');
insert into pokemon_species (name) values ('Big Voice');
insert into pokemon_species (name) values ('Big-Hearted');
insert into pokemon_species (name) values ('Bird');
insert into pokemon_species (name) values ('Bivalve');
insert into pokemon_species (name) values ('Blade');
insert into pokemon_species (name) values ('Blade Quill');
insert into pokemon_species (name) values ('Blast');
insert into pokemon_species (name) values ('Blast Turtle');
insert into pokemon_species (name) values ('Blaze');
insert into pokemon_species (name) values ('Blazing');
insert into pokemon_species (name) values ('Blimp');
insert into pokemon_species (name) values ('Bloom Sickle');
insert into pokemon_species (name) values ('Blossom');
insert into pokemon_species (name) values ('Bolt Strike');
insert into pokemon_species (name) values ('Bone Keeper');
insert into pokemon_species (name) values ('Bone Vulture');
insert into pokemon_species (name) values ('Bonsai');
insert into pokemon_species (name) values ('Bounce');
insert into pokemon_species (name) values ('Boundary');
insert into pokemon_species (name) values ('Bouquet');
insert into pokemon_species (name) values ('Boxing');
insert into pokemon_species (name) values ('Bright');
insert into pokemon_species (name) values ('Bronze');
insert into pokemon_species (name) values ('Bronze Bell');
insert into pokemon_species (name) values ('Brutal');
insert into pokemon_species (name) values ('Brutal Star');
insert into pokemon_species (name) values ('Bubble Frog');
insert into pokemon_species (name) values ('Bubble Jet');
insert into pokemon_species (name) values ('Bud');
insert into pokemon_species (name) values ('Bug Catcher');
insert into pokemon_species (name) values ('Bugle Beak');
insert into pokemon_species (name) values ('Bulb');
insert into pokemon_species (name) values ('Butterfly');

-- C
insert into pokemon_species (name) values ('Cactus');
insert into pokemon_species (name) values ('Candle');
insert into pokemon_species (name) values ('Cannon');
insert into pokemon_species (name) values ('Carefree');
insert into pokemon_species (name) values ('Caring');
insert into pokemon_species (name) values ('Cat Ferret');
insert into pokemon_species (name) values ('Catty');
insert into pokemon_species (name) values ('Cavalry');
insert into pokemon_species (name) values ('Cave');
insert into pokemon_species (name) values ('Cavern');
insert into pokemon_species (name) values ('Cell');
insert into pokemon_species (name) values ('Centipede');
insert into pokemon_species (name) values ('Cerebral');
insert into pokemon_species (name) values ('Cheering');
insert into pokemon_species (name) values ('Cherry');
insert into pokemon_species (name) values ('Chick');
insert into pokemon_species (name) values ('Chimp');
insert into pokemon_species (name) values ('Chill');
insert into pokemon_species (name) values ('Chinchilla');
insert into pokemon_species (name) values ('Clamping');
insert into pokemon_species (name) values ('Clap');
insert into pokemon_species (name) values ('Classy Cat');
insert into pokemon_species (name) values ('Clay Doll');
insert into pokemon_species (name) values ('Clear Wing');
insert into pokemon_species (name) values ('Coal');
insert into pokemon_species (name) values ('Cobra');
insert into pokemon_species (name) values ('Coconut');
insert into pokemon_species (name) values ('Cocoon');
insert into pokemon_species (name) values ('Coffin');
insert into pokemon_species (name) values ('Collective');
insert into pokemon_species (name) values ('Color Swap');
insert into pokemon_species (name) values ('Colossal');
insert into pokemon_species (name) values ('Colt');
insert into pokemon_species (name) values ('Compass');
insert into pokemon_species (name) values ('Compressed');
insert into pokemon_species (name) values ('Constraint');
insert into pokemon_species (name) values ('Continent');
insert into pokemon_species (name) values ('Coral');
insert into pokemon_species (name) values ('Cotton Bird');
insert into pokemon_species (name) values ('Cotton Candy');
insert into pokemon_species (name) values ('Cotton Puff');
insert into pokemon_species (name) values ('Cottonweed');
insert into pokemon_species (name) values ('Courting');
insert into pokemon_species (name) values ('Cricket');
insert into pokemon_species (name) values ('Cruel');
insert into pokemon_species (name) values ('Crystallizing');
insert into pokemon_species (name) values ('Curlipede');
insert into pokemon_species (name) values ('Cyclone');

-- D
insert into pokemon_species (name) values ('Dancing');
insert into pokemon_species (name) values ('Dark');
insert into pokemon_species (name) values ('Darkness');
insert into pokemon_species (name) values ('Daunting');
insert into pokemon_species (name) values ('Deceiver');
insert into pokemon_species (name) values ('Deep Black');
insert into pokemon_species (name) values ('Deep Sea');
insert into pokemon_species (name) values ('Delivery');
insert into pokemon_species (name) values ('Desert Croc');
insert into pokemon_species (name) values ('Despot');
insert into pokemon_species (name) values ('Destruction');
insert into pokemon_species (name) values ('Devious');
insert into pokemon_species (name) values ('Diapered');
insert into pokemon_species (name) values ('Digging');
insert into pokemon_species (name) values ('Disaster');
insert into pokemon_species (name) values ('Discharge');
insert into pokemon_species (name) values ('Discipline');
insert into pokemon_species (name) values ('Disguise');
insert into pokemon_species (name) values ('Diving');
insert into pokemon_species (name) values ('Djinn');
insert into pokemon_species (name) values ('DNA');
insert into pokemon_species (name) values ('Donkey');
insert into pokemon_species (name) values ('Dopey');
insert into pokemon_species (name) values ('Draft Horse');
insert into pokemon_species (name) values ('Dragon');
insert into pokemon_species (name) values ('Drawn Sword');
insert into pokemon_species (name) values ('Dream Eater');
insert into pokemon_species (name) values ('Drill');
insert into pokemon_species (name) values ('Drowsing');
insert into pokemon_species (name) values ('Duck');

-- E
insert into pokemon_species (name) values ('Eaglet');
insert into pokemon_species (name) values ('Egg');
insert into pokemon_species (name) values ('Elder Tree');
insert into pokemon_species (name) values ('Electric');
insert into pokemon_species (name) values ('Electrified');
insert into pokemon_species (name) values ('EleFish');
insert into pokemon_species (name) values ('EleSpider');
insert into pokemon_species (name) values ('EleSquirrel');
insert into pokemon_species (name) values ('Emanation');
insert into pokemon_species (name) values ('Ember');
insert into pokemon_species (name) values ('Embrace');
insert into pokemon_species (name) values ('Emotion');
insert into pokemon_species (name) values ('Emperor');
insert into pokemon_species (name) values ('Endurance');
insert into pokemon_species (name) values ('Eon');
insert into pokemon_species (name) values ('Eruption');
insert into pokemon_species (name) values ('Evolution');
insert into pokemon_species (name) values ('Eyeball');

-- F
insert into pokemon_species (name) values ('Face');
insert into pokemon_species (name) values ('Fairy');
insert into pokemon_species (name) values ('Fang Scorpion');
insert into pokemon_species (name) values ('Fang Snake');
insert into pokemon_species (name) values ('Feeling');
insert into pokemon_species (name) values ('Fire Cat');
insert into pokemon_species (name) values ('Fire Horse');
insert into pokemon_species (name) values ('Fire Mouse');
insert into pokemon_species (name) values ('Fire Pig');
insert into pokemon_species (name) values ('Firefly');
insert into pokemon_species (name) values ('Fireworks');
insert into pokemon_species (name) values ('First Bird');
insert into pokemon_species (name) values ('Fish');
insert into pokemon_species (name) values ('Five Star');
insert into pokemon_species (name) values ('Fixation');
insert into pokemon_species (name) values ('Flailing');
insert into pokemon_species (name) values ('Flame');
insert into pokemon_species (name) values ('Flash');
insert into pokemon_species (name) values ('Float Whale');
insert into pokemon_species (name) values ('Floating');
insert into pokemon_species (name) values ('Flower');
insert into pokemon_species (name) values ('Flowering');
insert into pokemon_species (name) values ('Flycatcher');
insert into pokemon_species (name) values ('Flyscorpion');
insert into pokemon_species (name) values ('Forbidden');
insert into pokemon_species (name) values ('Forest');
insert into pokemon_species (name) values ('Formidable');
insert into pokemon_species (name) values ('Fossil');
insert into pokemon_species (name) values ('Fox');
insert into pokemon_species (name) values ('Fragance');
insert into pokemon_species (name) values ('Freeze');
insert into pokemon_species (name) values ('Freezing');
insert into pokemon_species (name) values ('Fresh Snow');
insert into pokemon_species (name) values ('Frog');
insert into pokemon_species (name) values ('Frost Tree');
insert into pokemon_species (name) values ('Fruit');

-- G
insert into pokemon_species (name) values ('Garden');
insert into pokemon_species (name) values ('Gas');
insert into pokemon_species (name) values ('Gear');
insert into pokemon_species (name) values ('Generator');
insert into pokemon_species (name) values ('Genetic');
insert into pokemon_species (name) values ('Geyser');
insert into pokemon_species (name) values ('Gleam Eyes');
insert into pokemon_species (name) values ('Gloomdweller');
insert into pokemon_species (name) values ('Glowing');
insert into pokemon_species (name) values ('Gnash Teeth');
insert into pokemon_species (name) values ('Goldfish');
insert into pokemon_species (name) values ('Grass Monkey');
insert into pokemon_species (name) values ('Grass Quill');
insert into pokemon_species (name) values ('Grass Snake');
insert into pokemon_species (name) values ('Grassland');
insert into pokemon_species (name) values ('Gratitude');
insert into pokemon_species (name) values ('Gripper');
insert into pokemon_species (name) values ('Grove');
insert into pokemon_species (name) values ('Guts');

-- H
insert into pokemon_species (name) values ('Hairy Bug');
insert into pokemon_species (name) values ('Handstand');
insert into pokemon_species (name) values ('Happiness');
insert into pokemon_species (name) values ('Hard Scale');
insert into pokemon_species (name) values ('Hard Shell');
insert into pokemon_species (name) values ('Head Butt');
insert into pokemon_species (name) values ('Hearing');
insert into pokemon_species (name) values ('Heavyweight');
insert into pokemon_species (name) values ('Heel');
insert into pokemon_species (name) values ('Herb');
insert into pokemon_species (name) values ('Hermit Crab');
insert into pokemon_species (name) values ('Hibernator');
insert into pokemon_species (name) values ('High Temp');
insert into pokemon_species (name) values ('Hippo');
insert into pokemon_species (name) values ('Hoodlum');
insert into pokemon_species (name) values ('Hostile');
insert into pokemon_species (name) values ('Howitzer');
insert into pokemon_species (name) values ('Human Shape');
insert into pokemon_species (name) values ('Humming');

-- I
insert into pokemon_species (name) values ('Ice Break');
insert into pokemon_species (name) values ('Ice Chunk');
insert into pokemon_species (name) values ('Iceberg');
insert into pokemon_species (name) values ('Icy Snow');
insert into pokemon_species (name) values ('Illuminating');
insert into pokemon_species (name) values ('Illusion Fox');
insert into pokemon_species (name) values ('Imitation');
insert into pokemon_species (name) values ('Insect');
insert into pokemon_species (name) values ('Intertwining');
insert into pokemon_species (name) values ('Intimidation');
insert into pokemon_species (name) values ('Irate');
insert into pokemon_species (name) values ('Iron');
insert into pokemon_species (name) values ('Iron Ant');
insert into pokemon_species (name) values ('Iron Armor');
insert into pokemon_species (name) values ('Iron Ball');
insert into pokemon_species (name) values ('Iron Claw');
insert into pokemon_species (name) values ('Iron Leg');
insert into pokemon_species (name) values ('Iron Snake');
insert into pokemon_species (name) values ('Iron Will');

-- J
insert into pokemon_species (name) values ('Jellyfish');
insert into pokemon_species (name) values ('Jet');
insert into pokemon_species (name) values ('Jewel');
insert into pokemon_species (name) values ('Jolly');
insert into pokemon_species (name) values ('Jubilee');
insert into pokemon_species (name) values ('Judo');
insert into pokemon_species (name) values ('Junkivore');

-- K
insert into pokemon_species (name) values ('Karate');
insert into pokemon_species (name) values ('Key Ring');
insert into pokemon_species (name) values ('Kicking');
insert into pokemon_species (name) values ('Kiss');
insert into pokemon_species (name) values ('Kite');
insert into pokemon_species (name) values ('Kitten');
insert into pokemon_species (name) values ('Knowlege');

-- L
insert into pokemon_species (name) values ('Lamp');
insert into pokemon_species (name) values ('Land Shark');
insert into pokemon_species (name) values ('Land Snake');
insert into pokemon_species (name) values ('Land Spirit');
insert into pokemon_species (name) values ('Larva');
insert into pokemon_species (name) values ('Launch');
insert into pokemon_species (name) values ('Lava');
insert into pokemon_species (name) values ('Lava Dome');
insert into pokemon_species (name) values ('Lazy');
insert into pokemon_species (name) values ('Leaf');
insert into pokemon_species (name) values ('Leaf-Wrapped');
insert into pokemon_species (name) values ('Legendary');
insert into pokemon_species (name) values ('Licking');
insert into pokemon_species (name) values ('Life');
insert into pokemon_species (name) values ('Light');
insert into pokemon_species (name) values ('Lightning');
insert into pokemon_species (name) values ('Lion Cub');
insert into pokemon_species (name) values ('Lissome');
insert into pokemon_species (name) values ('Little Bear');
insert into pokemon_species (name) values ('Live Coal');
insert into pokemon_species (name) values ('Lizard');
insert into pokemon_species (name) values ('Loitering');
insert into pokemon_species (name) values ('Lonely');
insert into pokemon_species (name) values ('Longevity');
insert into pokemon_species (name) values ('Long Body');
insert into pokemon_species (name) values ('Long Leg');
insert into pokemon_species (name) values ('Long Neck');
insert into pokemon_species (name) values ('Long Nose');
insert into pokemon_species (name) values ('Long Tail');
insert into pokemon_species (name) values ('Lookout');
insert into pokemon_species (name) values ('Loud Noise');
insert into pokemon_species (name) values ('Loyal Dog');
insert into pokemon_species (name) values ('Lunar');
insert into pokemon_species (name) values ('Luring');

-- M
insert into pokemon_species (name) values ('Mach');
insert into pokemon_species (name) values ('Magical');
insert into pokemon_species (name) values ('Magnet');
insert into pokemon_species (name) values ('Magnet Area');
insert into pokemon_species (name) values ('Manipulate');
insert into pokemon_species (name) values ('Mantis');
insert into pokemon_species (name) values ('Mantle');
insert into pokemon_species (name) values ('Marionette');
insert into pokemon_species (name) values ('Martial Arts');
insert into pokemon_species (name) values ('Meditate');
insert into pokemon_species (name) values ('Mega Fire Pig');
insert into pokemon_species (name) values ('Megapede');
insert into pokemon_species (name) values ('Megaton');
insert into pokemon_species (name) values ('Melody');
insert into pokemon_species (name) values ('Meringue');
insert into pokemon_species (name) values ('Meteor');
insert into pokemon_species (name) values ('Meteorite');
insert into pokemon_species (name) values ('Milk Cow');
insert into pokemon_species (name) values ('Mime');
insert into pokemon_species (name) values ('Mischief');
insert into pokemon_species (name) values ('Mitosis');
insert into pokemon_species (name) values ('Mock Kelp');
insert into pokemon_species (name) values ('Mold');
insert into pokemon_species (name) values ('Mole');
insert into pokemon_species (name) values ('Moone');
insert into pokemon_species (name) values ('Moonlight');
insert into pokemon_species (name) values ('Moth');
insert into pokemon_species (name) values ('Mount');
insert into pokemon_species (name) values ('Mouse');
insert into pokemon_species (name) values ('Mud Fish');
insert into pokemon_species (name) values ('Multiplying');
insert into pokemon_species (name) values ('Muscular');
insert into pokemon_species (name) values ('Mushroom');
insert into pokemon_species (name) values ('Music Note');
insert into pokemon_species (name) values ('Mysterious');
insert into pokemon_species (name) values ('Mystic');

-- N
insert into pokemon_species (name) values ('Nebula');
insert into pokemon_species (name) values ('Neon');
insert into pokemon_species (name) values ('New Species');
insert into pokemon_species (name) values ('Ninja');
insert into pokemon_species (name) values ('Numb');
insert into pokemon_species (name) values ('Nurturing');

-- O
insert into pokemon_species (name) values ('Ogre Darner');
insert into pokemon_species (name) values ('Ogre Scorpion');
insert into pokemon_species (name) values ('Old Shrimp');
insert into pokemon_species (name) values ('Order');
insert into pokemon_species (name) values ('Ore');
insert into pokemon_species (name) values ('Overturning');
insert into pokemon_species (name) values ('Owl');

-- P
insert into pokemon_species (name) values ('Painter');
insert into pokemon_species (name) values ('Paleozoic');
insert into pokemon_species (name) values ('Parasite');
insert into pokemon_species (name) values ('Parent');
insert into pokemon_species (name) values ('Patient');
insert into pokemon_species (name) values ('Penguin');
insert into pokemon_species (name) values ('Perfume');
insert into pokemon_species (name) values ('Pig');
insert into pokemon_species (name) values ('Pig Monkey');
insert into pokemon_species (name) values ('Pincer');
insert into pokemon_species (name) values ('Pitch-Black');
insert into pokemon_species (name) values ('Placid');
insert into pokemon_species (name) values ('Plasma');
insert into pokemon_species (name) values ('Plate');
insert into pokemon_species (name) values ('Playful');
insert into pokemon_species (name) values ('Playhouse');
insert into pokemon_species (name) values ('Plump Mouse');
insert into pokemon_species (name) values ('Poison Bag');
insert into pokemon_species (name) values ('Poison Bee');
insert into pokemon_species (name) values ('Poison Gas');
insert into pokemon_species (name) values ('Poison Moth');
insert into pokemon_species (name) values ('Poison Pin');
insert into pokemon_species (name) values ('Polka Dot');
insert into pokemon_species (name) values ('Pond Skater');
insert into pokemon_species (name) values ('Poodle');
insert into pokemon_species (name) values ('Pop Star');
insert into pokemon_species (name) values ('Posy Picker');
insert into pokemon_species (name) values ('Predator');
insert into pokemon_species (name) values ('Prim');
insert into pokemon_species (name) values ('Prism');
insert into pokemon_species (name) values ('Protostar');
insert into pokemon_species (name) values ('Prototurtle');
insert into pokemon_species (name) values ('Proud');
insert into pokemon_species (name) values ('Psi');
insert into pokemon_species (name) values ('Pumpkin');
insert into pokemon_species (name) values ('Punching');
insert into pokemon_species (name) values ('Puppet');
insert into pokemon_species (name) values ('Puppy');

-- R
insert into pokemon_species (name) values ('Rabbit');
insert into pokemon_species (name) values ('Rainbow');
insert into pokemon_species (name) values ('Rampart');
insert into pokemon_species (name) values ('Regal');
insert into pokemon_species (name) values ('Rendezvous');
insert into pokemon_species (name) values ('Renegade');
insert into pokemon_species (name) values ('Requiem');
insert into pokemon_species (name) values ('Restraint');
insert into pokemon_species (name) values ('Revolving');
insert into pokemon_species (name) values ('River Crab');
insert into pokemon_species (name) values ('Rock');
insert into pokemon_species (name) values ('Rock Head');
insert into pokemon_species (name) values ('Rock Inn');
insert into pokemon_species (name) values ('Rock Peak');
insert into pokemon_species (name) values ('Rock Skin');
insert into pokemon_species (name) values ('Rock Snake');
insert into pokemon_species (name) values ('Rogue');
insert into pokemon_species (name) values ('Roly-Poly');
insert into pokemon_species (name) values ('Royal');
insert into pokemon_species (name) values ('Royal Heir');
insert into pokemon_species (name) values ('Royal Sword');
insert into pokemon_species (name) values ('Ruffian');
insert into pokemon_species (name) values ('Rushing');

-- S
insert into pokemon_species (name) values ('Sage');
insert into pokemon_species (name) values ('Sand Castle');
insert into pokemon_species (name) values ('Sand Heap');
insert into pokemon_species (name) values ('Savage');
insert into pokemon_species (name) values ('Scale');
insert into pokemon_species (name) values ('Scaly');
insert into pokemon_species (name) values ('Scarecrow');
insert into pokemon_species (name) values ('Scarf');
insert into pokemon_species (name) values ('Scatterdust');
insert into pokemon_species (name) values ('Scorching');
insert into pokemon_species (name) values ('Scorpion');
insert into pokemon_species (name) values ('Scout');
insert into pokemon_species (name) values ('Scratch Cat');
insert into pokemon_species (name) values ('Screech');
insert into pokemon_species (name) values ('Scuffle');
insert into pokemon_species (name) values ('Sea Basin');
insert into pokemon_species (name) values ('Sea Creeper');
insert into pokemon_species (name) values ('Sea Cucumber');
insert into pokemon_species (name) values ('Sea Drifter');
insert into pokemon_species (name) values ('Sea Lily');
insert into pokemon_species (name) values ('Sea Lion');
insert into pokemon_species (name) values ('Sea Otter');
insert into pokemon_species (name) values ('Sea Slug');
insert into pokemon_species (name) values ('Sea Weasel');
insert into pokemon_species (name) values ('Seafaring');
insert into pokemon_species (name) values ('Seafaring');
insert into pokemon_species (name) values ('Season');
insert into pokemon_species (name) values ('Seed');
insert into pokemon_species (name) values ('Sewing');
insert into pokemon_species (name) values ('Shadow');
insert into pokemon_species (name) values ('Sharp Blade');
insert into pokemon_species (name) values ('Sharp Claw');
insert into pokemon_species (name) values ('Shed');
insert into pokemon_species (name) values ('Shedding');
insert into pokemon_species (name) values ('Shell Out');
insert into pokemon_species (name) values ('Shellfish');
insert into pokemon_species (name) values ('Shield');
insert into pokemon_species (name) values ('Sickle Grass');
insert into pokemon_species (name) values ('Single Bloom');
insert into pokemon_species (name) values ('Single Horn');
insert into pokemon_species (name) values ('Skunk');
insert into pokemon_species (name) values ('Sky High');
insert into pokemon_species (name) values ('Sky Squirrel');
insert into pokemon_species (name) values ('Slacker');
insert into pokemon_species (name) values ('Sleeping');
insert into pokemon_species (name) values ('Sludge');
insert into pokemon_species (name) values ('Small Fry');
insert into pokemon_species (name) values ('Snail');
insert into pokemon_species (name) values ('Snake');
insert into pokemon_species (name) values ('Snow Hat');
insert into pokemon_species (name) values ('Snow Land');
insert into pokemon_species (name) values ('Snowstorm');
insert into pokemon_species (name) values ('Soft Tissue');
insert into pokemon_species (name) values ('Soloist');
insert into pokemon_species (name) values ('Sound Wave');
insert into pokemon_species (name) values ('South Sea');
insert into pokemon_species (name) values ('Spatial');
insert into pokemon_species (name) values ('Spark');
insert into pokemon_species (name) values ('Spike Ball');
insert into pokemon_species (name) values ('Spikes');
insert into pokemon_species (name) values ('Spiny Armor');
insert into pokemon_species (name) values ('Spiny Nut');
insert into pokemon_species (name) values ('Spiral');
insert into pokemon_species (name) values ('Spirit');
insert into pokemon_species (name) values ('Spitfire');
insert into pokemon_species (name) values ('Spot Panda');
insert into pokemon_species (name) values ('Spray');
insert into pokemon_species (name) values ('Stag Beetle');
insert into pokemon_species (name) values ('Stakeout');
insert into pokemon_species (name) values ('Star Shape');
insert into pokemon_species (name) values ('Starling');
insert into pokemon_species (name) values ('Steam');
insert into pokemon_species (name) values ('Stomach');
insert into pokemon_species (name) values ('Stone Home');
insert into pokemon_species (name) values ('String Spit');
insert into pokemon_species (name) values ('Strong Arm');
insert into pokemon_species (name) values ('Stump');
insert into pokemon_species (name) values ('Subterrene');
insert into pokemon_species (name) values ('Sun');
insert into pokemon_species (name) values ('Sunne');
insert into pokemon_species (name) values ('Superpower');
insert into pokemon_species (name) values ('Swallow');
insert into pokemon_species (name) values ('Swine');
insert into pokemon_species (name) values ('Swollen');
insert into pokemon_species (name) values ('Sword');
insert into pokemon_species (name) values ('Sword Blade');
insert into pokemon_species (name) values ('Symbol');
insert into pokemon_species (name) values ('Synthetic');

-- T
insert into pokemon_species (name) values ('Tadpole');
insert into pokemon_species (name) values ('Teamwork');
insert into pokemon_species (name) values ('Temporal');
insert into pokemon_species (name) values ('Tender');
insert into pokemon_species (name) values ('Thorn');
insert into pokemon_species (name) values ('Thorn Monkey');
insert into pokemon_species (name) values ('Thorn Pod');
insert into pokemon_species (name) values ('Thorn Seed');
insert into pokemon_species (name) values ('Thunder');
insert into pokemon_species (name) values ('Thunderbolt');
insert into pokemon_species (name) values ('Tiger Cat');
insert into pokemon_species (name) values ('Time Travel');
insert into pokemon_species (name) values ('Tiny Bee');
insert into pokemon_species (name) values ('Tiny Bird');
insert into pokemon_species (name) values ('Tiny Leaf');
insert into pokemon_species (name) values ('Tiny Mouse');
insert into pokemon_species (name) values ('Tiny Pigeon');
insert into pokemon_species (name) values ('Tiny Raccoon');
insert into pokemon_species (name) values ('Tiny Robin');
insert into pokemon_species (name) values ('Tiny Swallow');
insert into pokemon_species (name) values ('Tiny Turtle');
insert into pokemon_species (name) values ('Torch');
insert into pokemon_species (name) values ('Toxic Lizard');
insert into pokemon_species (name) values ('Toxic Mouth');
insert into pokemon_species (name) values ('Trainee');
insert into pokemon_species (name) values ('Transform');
insert into pokemon_species (name) values ('Transport');
insert into pokemon_species (name) values ('Trap');
insert into pokemon_species (name) values ('Trash Bag');
insert into pokemon_species (name) values ('Trash Heap');
insert into pokemon_species (name) values ('Tricky Fox');
insert into pokemon_species (name) values ('Triple Bird');
insert into pokemon_species (name) values ('Tundra');
insert into pokemon_species (name) values ('Turn Tail');
insert into pokemon_species (name) values ('Turtle');
insert into pokemon_species (name) values ('Tusk');
insert into pokemon_species (name) values ('Twin Bird');
insert into pokemon_species (name) values ('Twin Tusk');
insert into pokemon_species (name) values ('Two-Handed');

-- V
insert into pokemon_species (name) values ('Valiant');
insert into pokemon_species (name) values ('Vast White');
insert into pokemon_species (name) values ('Verdant');
insert into pokemon_species (name) values ('Vibration');
insert into pokemon_species (name) values ('Victory');
insert into pokemon_species (name) values ('Vine');
insert into pokemon_species (name) values ('Virtual');
insert into pokemon_species (name) values ('Volcano');

-- W
insert into pokemon_species (name) values ('Water Bird');
insert into pokemon_species (name) values ('Water Bubble');
insert into pokemon_species (name) values ('Water Fish');
insert into pokemon_species (name) values ('Water Gun');
insert into pokemon_species (name) values ('Water Weed');
insert into pokemon_species (name) values ('Weather');
insert into pokemon_species (name) values ('Weed');
insert into pokemon_species (name) values ('Whiskers');
insert into pokemon_species (name) values ('Whisper');
insert into pokemon_species (name) values ('White Bird');
insert into pokemon_species (name) values ('Wicked');
insert into pokemon_species (name) values ('Wild Bull');
insert into pokemon_species (name) values ('Wild Duck');
insert into pokemon_species (name) values ('Wild Monkey');
insert into pokemon_species (name) values ('Wild Pigeon');
insert into pokemon_species (name) values ('Willpower');
insert into pokemon_species (name) values ('Wily');
insert into pokemon_species (name) values ('Wind Chime');
insert into pokemon_species (name) values ('Windveiled');
insert into pokemon_species (name) values ('Wing Fish');
insert into pokemon_species (name) values ('Wish');
insert into pokemon_species (name) values ('Wolf');
insert into pokemon_species (name) values ('Wood Gecko');
insert into pokemon_species (name) values ('Woodpecker');
insert into pokemon_species (name) values ('Wool');
insert into pokemon_species (name) values ('Woolly Crab');
insert into pokemon_species (name) values ('Worm');
insert into pokemon_species (name) values ('Wrestling');

-- Y
insert into pokemon_species (name)  values ('Young Fowl');

-- Z
insert into pokemon_species (name)  values ('Zen Charm');

-- pokemon_type
insert into pokemon_type (name) values ('Bug');
insert into pokemon_type (name) values ('Dark');
insert into pokemon_type (name) values ('Dragon');
insert into pokemon_type (name) values ('Electric');
insert into pokemon_type (name) values ('Fairy');
insert into pokemon_type (name) values ('Fighting');
insert into pokemon_type (name) values ('Fire');
insert into pokemon_type (name) values ('Flying');
insert into pokemon_type (name) values ('Ghost');
insert into pokemon_type (name) values ('Grass');
insert into pokemon_type (name) values ('Ground');
insert into pokemon_type (name) values ('Ice');
insert into pokemon_type (name) values ('Normal');
insert into pokemon_type (name) values ('Poison');
insert into pokemon_type (name) values ('Psychic');
insert into pokemon_type (name) values ('Rock');
insert into pokemon_type (name) values ('Steel');
insert into pokemon_type (name) values ('Water');

-- pokemon_stats
insert into pokemon_stats (name) values ('HP');
insert into pokemon_stats (name) values ('Attack');
insert into pokemon_stats (name) values ('Defense');
insert into pokemon_stats (name) values ('Special Attack');
insert into pokemon_stats (name) values ('Special Defense');
insert into pokemon_stats (name) values ('Speed');

-- pokemon forms

-- Deoxys
insert into pokemon_form (form_type, is_mega_evolution) values ('Normal Deoxys', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Attack Deoxys', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Defense Deoxys', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Speed Deoxys', false);

-- Burmy/Wormadam
insert into pokemon_form (form_type, is_mega_evolution) values ('Plant Cloak', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sand Cloak', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Trash Cloak', false);

-- Rotom
insert into pokemon_form (form_type, is_mega_evolution) values ('Basic Rotom', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Mow Rotom', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Frost Rotom', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Heat Rotom', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Fan Rotom', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Wash Rotom', false);

-- Giratina
insert into pokemon_form (form_type, is_mega_evolution) values ('Altered Forme', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Origin Forme', false);

-- Shaymin
insert into pokemon_form (form_type, is_mega_evolution) values ('Land Forme', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sky Forme', false);

-- Basculin
-- Tem Itens!!!!
insert into pokemon_form (form_type, is_mega_evolution) values ('Red Stripe Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Blue Stripe Form', false);

-- Darmanitan
insert into pokemon_form (form_type, is_mega_evolution) values ('Normal Mode', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Zen Mode', false);

-- Deerling/Sawsbuck
insert into pokemon_form (form_type, is_mega_evolution) values ('Spring Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Summer Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Autumn Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Winter Form', false);

-- Tornadus/Thundurus/Landorus
insert into pokemon_form (form_type, is_mega_evolution) values ('Incarnation Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Therian Form', false);

-- Kyurem
insert into pokemon_form (form_type, is_mega_evolution) values ('Basic Kyurem', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Black Kyurem', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('White Kyurem', false);

-- Keldeo
insert into pokemon_form (form_type, is_mega_evolution) values ('Usual Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Resolution Form', false);

-- Meloetta
insert into pokemon_form (form_type, is_mega_evolution) values ('Aria Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Pirouette Form', false);

-- Greninja
insert into pokemon_form (form_type, is_mega_evolution) values ('Ash-Greninja', false);

-- Aegislash
insert into pokemon_form (form_type, is_mega_evolution) values ('Shield Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Blade Form', false);

-- Pumpkaboo/Gourgeist
insert into pokemon_form (form_type, is_mega_evolution) values ('Small Size', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Average Size', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Large Size', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Super Size', false);

-- Zygarde
insert into pokemon_form (form_type, is_mega_evolution) values ('10% Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('50% Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Complete Form', false);

-- Hoopa
insert into pokemon_form (form_type, is_mega_evolution) values ('Hoopa Confined', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Hoopa Unbound', false);

-- Oricorio
-- Tem Itens!!!!
insert into pokemon_form (form_type, is_mega_evolution) values ('Pom-Pom Style', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Baile Style', false);
insert into pokemon_form (form_type, is_mega_evolution) values ("P'au Style", false); -- pode dar pau
insert into pokemon_form (form_type, is_mega_evolution) values ('Sensu Style', false);

-- Lycanroc
insert into pokemon_form (form_type, is_mega_evolution) values ('Midday Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Midnight Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Dusk Form', false);

-- Wishiwashi
insert into pokemon_form (form_type, is_mega_evolution) values ('Solo Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('School Form', false);

-- Minior
-- Blue Core
-- Green Core
-- Indigo Core
-- Orange Core
-- Red Core
-- Violet Core
-- Yellow Core
insert into pokemon_form (form_type, is_mega_evolution) values ('Meteorite Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Core Form', false);

-- Necrozma
-- possui itens
insert into pokemon_form (form_type, is_mega_evolution) values ('Dusk Mane', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Dawn Wings', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Ultra Necrozma', false);

-- Unown
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown A', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown B', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown C', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown D', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown E', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown F', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown G', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown H', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown I', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown J', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown K', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown L', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown M', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown N', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown O', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown P', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown Q', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown R', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown S', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown T', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown U', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown V', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown W', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown X', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown Y', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown Z', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown ?', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Unown !', false);

-- Castform
-- create field description
insert into pokemon_form (form_type, is_mega_evolution) values ('Normal Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sunny Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Rainy Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Snowy Form', false);

-- Cherrim
insert into pokemon_form (form_type, is_mega_evolution) values ('Overcast Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sunshine Form', false);

-- Shellos/Gastrodon
insert into pokemon_form (form_type, is_mega_evolution) values ('West Sea', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('East Sea', false);

-- Arceus
-- possui itens!!!!
insert into pokemon_form (form_type, is_mega_evolution) values ('Normal Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Bug Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Dark Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Dragon Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Electric Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Fairy Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Fight Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Fire Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Flying Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Ghost Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Grass Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Ground Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Ice Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Poison Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Psychic Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Rock Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Steel Arceus', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Water Arceus', false);

-- Vivillon
insert into pokemon_form (form_type, is_mega_evolution) values ('Meadow Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Polar Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Tundra Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Continental Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Garden Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Elegant Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Icy Snow Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Modern Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Marine Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Archipelago Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('High Plains Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sandstorm Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('River Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Monsoon Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Savanna Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Sun Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Ocean Pattern', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Jungle Pattern', false);

-- Flabébé/Floette/Florges
insert into pokemon_form (form_type, is_mega_evolution) values ('Red Flower', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Blue Flower', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Orange Flower', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('White Flower', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Yellow Flower', false);

-- Furfrou
insert into pokemon_form (form_type, is_mega_evolution) values ('Natural Form', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Heart Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Star Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Diamond Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Deputante Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Matron Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Dandy Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('La Reine Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Kabuki Trim', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Pharaoh Trim', false);

-- Silvally
-- possui itens!!!!
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Normal', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Bug', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Dark', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Dragon', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Electric', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Fairy', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Fight', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Fire', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Flying', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Ghost', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Grass', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Ground', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Ice', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Poison', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Psychic', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Rock', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Steel', false);
insert into pokemon_form (form_type, is_mega_evolution) values ('Type: Water', false);

-- pokemon_ability
-- A
insert into pokemon_ability (name) values ('Adaptability');
insert into pokemon_ability (name) values ('Aerilate');
insert into pokemon_ability (name) values ('Aftermath');
insert into pokemon_ability (name) values ('Air Lock');
insert into pokemon_ability (name) values ('Analytic');
insert into pokemon_ability (name) values ('Anger Point');
insert into pokemon_ability (name) values ('Anticipation');
insert into pokemon_ability (name) values ('Arena Trap');
insert into pokemon_ability (name) values ('Aroma Veil');
insert into pokemon_ability (name) values ('Aura Break');

-- B
insert into pokemon_ability (name) values ('Bad Dreams');
insert into pokemon_ability (name) values ('Battery');
insert into pokemon_ability (name) values ('Battle Armor');
insert into pokemon_ability (name) values ('Battle Bond');
insert into pokemon_ability (name) values ('Beast Boost');
insert into pokemon_ability (name) values ('Berserk');
insert into pokemon_ability (name) values ('Big Pecks');
insert into pokemon_ability (name) values ('Blaze');
insert into pokemon_ability (name) values ('Bulletproof');

-- C
insert into pokemon_ability (name) values ('Cacophony');
insert into pokemon_ability (name) values ('Cheek Pouch');
insert into pokemon_ability (name) values ('Chlorophyll');
insert into pokemon_ability (name) values ('Clear Body');
insert into pokemon_ability (name) values ('Cloud Nine');
insert into pokemon_ability (name) values ('Color Change');
insert into pokemon_ability (name) values ('Comatose');
insert into pokemon_ability (name) values ('Competitive');
insert into pokemon_ability (name) values ('Compound Eyes');
insert into pokemon_ability (name) values ('Contrary');
insert into pokemon_ability (name) values ('Corrosion');
insert into pokemon_ability (name) values ('Cursed Body');
insert into pokemon_ability (name) values ('Cute Charm');

-- D
insert into pokemon_ability (name) values ('Damp');
insert into pokemon_ability (name) values ('Dancer');
insert into pokemon_ability (name) values ('Dark Aura');
insert into pokemon_ability (name) values ('Dazzling');
insert into pokemon_ability (name) values ('Defeatist');
insert into pokemon_ability (name) values ('Defiant');
insert into pokemon_ability (name) values ('Delta Stream');
insert into pokemon_ability (name) values ('Desolate Land');
insert into pokemon_ability (name) values ('Disguise');
insert into pokemon_ability (name) values ('Download');
insert into pokemon_ability (name) values ('Drizzle');
insert into pokemon_ability (name) values ('Drought');
insert into pokemon_ability (name) values ('Dry Skin');

-- E
insert into pokemon_ability (name) values ('Early Bird');
insert into pokemon_ability (name) values ('Effect Spore');
insert into pokemon_ability (name) values ('Electric Surge');
insert into pokemon_ability (name) values ('Emergency Exit');

-- F
insert into pokemon_ability (name) values ('Fairy Aura');
insert into pokemon_ability (name) values ('Filter');
insert into pokemon_ability (name) values ('Flame Body');
insert into pokemon_ability (name) values ('Flare Boost');
insert into pokemon_ability (name) values ('Flash Fire');
insert into pokemon_ability (name) values ('Flower Gift');
insert into pokemon_ability (name) values ('Flower Veil');
insert into pokemon_ability (name) values ('Fluffy');
insert into pokemon_ability (name) values ('Forecast');
insert into pokemon_ability (name) values ('Forewarn');
insert into pokemon_ability (name) values ('Friend Guard');
insert into pokemon_ability (name) values ('Frisk');
insert into pokemon_ability (name) values ('Full Metal Body');
insert into pokemon_ability (name) values ('Fur Coat');

-- G
insert into pokemon_ability (name) values ('Gale Wings');
insert into pokemon_ability (name) values ('Galvanize');
insert into pokemon_ability (name) values ('Gluttony');
insert into pokemon_ability (name) values ('Gooey');
insert into pokemon_ability (name) values ('Grass Pelt');
insert into pokemon_ability (name) values ('Grassy Surge');
insert into pokemon_ability (name) values ('Guts');

-- H
insert into pokemon_ability (name) values ('Harvest');
insert into pokemon_ability (name) values ('Healer');
insert into pokemon_ability (name) values ('Heatproof');
insert into pokemon_ability (name) values ('Heavy Metal');
insert into pokemon_ability (name) values ('Honey Gather');
insert into pokemon_ability (name) values ('Huge Power');
insert into pokemon_ability (name) values ('Hustle');
insert into pokemon_ability (name) values ('Hydration');
insert into pokemon_ability (name) values ('Hyper Cutter');

-- I
insert into pokemon_ability (name) values ('Ice Body');
insert into pokemon_ability (name) values ('Illuminate');
insert into pokemon_ability (name) values ('Illusion');
insert into pokemon_ability (name) values ('Immunity');
insert into pokemon_ability (name) values ('Imposter');
insert into pokemon_ability (name) values ('Infiltrator');
insert into pokemon_ability (name) values ('Innards Out');
insert into pokemon_ability (name) values ('Inner Focus');
insert into pokemon_ability (name) values ('Insomnia');
insert into pokemon_ability (name) values ('Intimidate');
insert into pokemon_ability (name) values ('Iron Barbs');
insert into pokemon_ability (name) values ('Iron Fist');

-- J
insert into pokemon_ability (name) values ('Justified');

-- K
insert into pokemon_ability (name) values ('Keen Eye');
insert into pokemon_ability (name) values ('Klutz');

-- L
insert into pokemon_ability (name) values ('Leaf Guard');
insert into pokemon_ability (name) values ('Levitate');
insert into pokemon_ability (name) values ('Light Metal');
insert into pokemon_ability (name) values ('Lightning Rod');
insert into pokemon_ability (name) values ('Limber');
insert into pokemon_ability (name) values ('Liquid Ooze');
insert into pokemon_ability (name) values ('Liquid Voice');
insert into pokemon_ability (name) values ('Long Reach');

-- M
insert into pokemon_ability (name) values ('Magic Bounce');
insert into pokemon_ability (name) values ('Magic Guard');
insert into pokemon_ability (name) values ('Magician');
insert into pokemon_ability (name) values ('Magma Armor');
insert into pokemon_ability (name) values ('Magnet Pull');
insert into pokemon_ability (name) values ('Marvel Scale');
insert into pokemon_ability (name) values ('Mega Launcher');
insert into pokemon_ability (name) values ('Merciless');
insert into pokemon_ability (name) values ('Minus');
insert into pokemon_ability (name) values ('Misty Surge');
insert into pokemon_ability (name) values ('Mold Breaker');
insert into pokemon_ability (name) values ('Moody');
insert into pokemon_ability (name) values ('Motor Drive');
insert into pokemon_ability (name) values ('Moxie');
insert into pokemon_ability (name) values ('Multiscale');
insert into pokemon_ability (name) values ('Multitype');
insert into pokemon_ability (name) values ('Mummy');

-- N
insert into pokemon_ability (name) values ('Natural Cure');
insert into pokemon_ability (name) values ('Neuroforce');
insert into pokemon_ability (name) values ('No Guard');
insert into pokemon_ability (name) values ('Normalize');

-- O
insert into pokemon_ability (name) values ('Oblivious');
insert into pokemon_ability (name) values ('Overcoat');
insert into pokemon_ability (name) values ('Overgrow');
insert into pokemon_ability (name) values ('Own Tempo');

-- P
insert into pokemon_ability (name) values ('Parental Bond');
insert into pokemon_ability (name) values ('Pickpocket');
insert into pokemon_ability (name) values ('Pickup');
insert into pokemon_ability (name) values ('Pixilate');
insert into pokemon_ability (name) values ('Plus');
insert into pokemon_ability (name) values ('Poison Heal');
insert into pokemon_ability (name) values ('Poison Point');
insert into pokemon_ability (name) values ('Poison Touch');
insert into pokemon_ability (name) values ('Power Construct');
insert into pokemon_ability (name) values ('Power of Alchemy');
insert into pokemon_ability (name) values ('Prankster');
insert into pokemon_ability (name) values ('Pressure');
insert into pokemon_ability (name) values ('Primordial Sea');
insert into pokemon_ability (name) values ('Prism Armor');
insert into pokemon_ability (name) values ('Protean');
insert into pokemon_ability (name) values ('Psychic Surge');
insert into pokemon_ability (name) values ('Pure Power');

-- Q
insert into pokemon_ability (name) values ('Queenly Majesty');
insert into pokemon_ability (name) values ('Quick Feet');

-- R
insert into pokemon_ability (name) values ('Rain Dish');
insert into pokemon_ability (name) values ('Rattled');
insert into pokemon_ability (name) values ('Receiver');
insert into pokemon_ability (name) values ('Reckless');
insert into pokemon_ability (name) values ('Refrigerate');
insert into pokemon_ability (name) values ('Regenerator');
insert into pokemon_ability (name) values ('Rivalry');
insert into pokemon_ability (name) values ('RKS System');
insert into pokemon_ability (name) values ('Rock Head');
insert into pokemon_ability (name) values ('Rough Skin');
insert into pokemon_ability (name) values ('Run Away');

-- S
insert into pokemon_ability (name) values ('Sand Force');
insert into pokemon_ability (name) values ('Sand Rush');
insert into pokemon_ability (name) values ('Sand Stream');
insert into pokemon_ability (name) values ('Sand Veil');
insert into pokemon_ability (name) values ('Sap Sipper');
insert into pokemon_ability (name) values ('Schooling');
insert into pokemon_ability (name) values ('Scrappy');
insert into pokemon_ability (name) values ('Serene Grace');
insert into pokemon_ability (name) values ('Shadow Shield');
insert into pokemon_ability (name) values ('Shadow Tag');
insert into pokemon_ability (name) values ('Shed Skin');
insert into pokemon_ability (name) values ('Sheer Force');
insert into pokemon_ability (name) values ('Shell Armor');
insert into pokemon_ability (name) values ('Shield Dust');
insert into pokemon_ability (name) values ('Shields Down');
insert into pokemon_ability (name) values ('Simple');
insert into pokemon_ability (name) values ('Skill Link');
insert into pokemon_ability (name) values ('Slow Start');
insert into pokemon_ability (name) values ('Slush Rush');
insert into pokemon_ability (name) values ('Sniper');
insert into pokemon_ability (name) values ('Snow Cloak');
insert into pokemon_ability (name) values ('Snow Warning');
insert into pokemon_ability (name) values ('Solar Power');
insert into pokemon_ability (name) values ('Solid Rock');
insert into pokemon_ability (name) values ('Soul-Heart');
insert into pokemon_ability (name) values ('Soundproof');
insert into pokemon_ability (name) values ('Speed Boost');
insert into pokemon_ability (name) values ('Stakeout');
insert into pokemon_ability (name) values ('Stall');
insert into pokemon_ability (name) values ('Stamina');
insert into pokemon_ability (name) values ('Stance Change');
insert into pokemon_ability (name) values ('Static');
insert into pokemon_ability (name) values ('Steadfast');
insert into pokemon_ability (name) values ('Steelworker');
insert into pokemon_ability (name) values ('Stench');
insert into pokemon_ability (name) values ('Sticky Hold');
insert into pokemon_ability (name) values ('Storm Drain');
insert into pokemon_ability (name) values ('Strong Jaw');
insert into pokemon_ability (name) values ('Sturdy');
insert into pokemon_ability (name) values ('Suction Cups');
insert into pokemon_ability (name) values ('Super Luck');
insert into pokemon_ability (name) values ('Surge Surfer');
insert into pokemon_ability (name) values ('Swarm');
insert into pokemon_ability (name) values ('Sweet Veil');
insert into pokemon_ability (name) values ('Swift Swim');
insert into pokemon_ability (name) values ('Symbiosis');
insert into pokemon_ability (name) values ('Synchronize');

-- T
insert into pokemon_ability (name) values ('Tangled Feet');
insert into pokemon_ability (name) values ('Tangling Hair');
insert into pokemon_ability (name) values ('Technician');
insert into pokemon_ability (name) values ('Telepathy');
insert into pokemon_ability (name) values ('Teravolt');
insert into pokemon_ability (name) values ('Thick Fat');
insert into pokemon_ability (name) values ('Tinted Lens');
insert into pokemon_ability (name) values ('Torrent');
insert into pokemon_ability (name) values ('Tough Claws');
insert into pokemon_ability (name) values ('Toxic Boost');
insert into pokemon_ability (name) values ('Trace');
insert into pokemon_ability (name) values ('Triage');
insert into pokemon_ability (name) values ('Truant');
insert into pokemon_ability (name) values ('Turboblaze');

-- U
insert into pokemon_ability (name) values ('Unaware');
insert into pokemon_ability (name) values ('Unburden');
insert into pokemon_ability (name) values ('Unnerve');

-- V
insert into pokemon_ability (name) values ('Victory Star');
insert into pokemon_ability (name) values ('Vital Spirit');
insert into pokemon_ability (name) values ('Volt Absorb');

-- W
insert into pokemon_ability (name) values ('Water Absorb');
insert into pokemon_ability (name) values ('Water Bubble');
insert into pokemon_ability (name) values ('Water Compaction');
insert into pokemon_ability (name) values ('Water Veil');
insert into pokemon_ability (name) values ('Weak Armor');
insert into pokemon_ability (name) values ('White Smoke');
insert into pokemon_ability (name) values ('Wimp Out');
insert into pokemon_ability (name) values ('Wonder Guard');
insert into pokemon_ability (name) values ('Wonder Skin');

-- Z
insert into pokemon_ability (name) values ('Zen Mode');


-- Pokemon
insert into pokemon (national_number
                        ,name
                        ,species_id
                        ,height
                        ,weight
                        ,base_experience) 
                    values (1
                            ,"Bulbasaur"
                            ,select species_id from pokemon_species where pokemon_species.name = "Seed"
                            ,"2,14"
                            ,"15.2 lbs"
                            ,64);