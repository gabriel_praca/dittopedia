import React from 'react';

import { Collapse, 
    Navbar, 
    NavbarToggler,
    NavbarBrand,
    Nav, 
    NavItem} from 'reactstrap';

import Login from '../pages/Login';
import DropdownLogin from '../pages/DropdownLogin';

class CustomNavbar extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div>
                <Navbar className='nav' light expand="md">
                    <NavbarBrand className='brand' href="/"> 
                        Dittopedia 
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                {/* <SearchBar /> */}
                            </NavItem>
                            <NavItem>
                                <Login />
                                {/* <DropdownLogin/> */}
                            </NavItem>
                            <NavItem>
                                {/* <Login /> */}
                                {/* <Button color="danger" onClick={Login.toggle}>{Login.props.buttonLabel}<FaUser /></Button> */}
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default CustomNavbar;