import React from 'react';

import { Card, 
    Button, 
    CardHeader, 
    CardFooter, 
    CardBody,
    CardTitle,
    CardText,
    Container,
    Row,
    Col,
    InputGroupText,
    Popover,
    PopoverHeader,
    PopoverBody,
    PopoverItem,
    Label} from 'reactstrap';

import PokeballIcon from '../components/PokeballIcon';


const pokemonUrl = 'http://localhost:3001/Pokemon';

class PokemonCard extends React.Component{
    constructor(props) {
      super(props);
  
      this.toggle = this.toggle.bind(this);
      this.state = {
        popoverOpen: false,

        //new
        data: [],
        isLoading: false,
        error: null,
        //new
      };
    }
    //new
    componentDidMount() {        
        this.setState({ isLoading: true });

        fetch(pokemonUrl + "/" + this.props.pokemonObj.pokemon_id + "/types")
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                else {
                    console.log(response);
                    throw new Error('Something went wrong ...');
                }
            })
            .then(response => this.setState({ data: response, isLoading: false }))
        .catch(error => this.setState({ error, isLoading: false }));
    }
    //new
    toggle() {
      this.setState({
        popoverOpen: !this.state.popoverOpen
      });
    }

    render(){
        return(
            <div className="pokemon-card">
                <CardHeader className="pokemon-card-header" id={'Popover-' + this.props.pokemonObj.pokemon_id} onClick={this.toggle}>
                    <Row>
                        <Col className="lg-2">
                            <PokeballIcon /> { this.props.pokemonObj.pokemon_id +" - " + this.props.pokemonObj.name}
                        </Col>
                    </Row>
                    <Row>
                        <Col className="lg-12">
                            {/* <a id={'Popover-' + this.props.id} onClick={this.toggle}> */}
                                <img className="pokemon-img" src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + 
                                            this.props.pokemonObj.pokemon_id + ".png"} alt={this.props.pokemonObj.name}/>
                            {/* </a> */}
                        </Col>
                    </Row>
                </CardHeader>
                <CardBody className="pokemon-card-body">
                    {/* <div className="pokemon-type-container"> */}
                    <Row>
                        <Col className="lg-2">
                        {/* <a href="#" className="pokemon-type-info pokemon-type-info-normal">Normal</a>  */}
                        {
                            this.state.data.map((d) => <Label className={"pokemon-type-info pokemon-type-info-" + d.name.toLowerCase()}>{d.name}</Label>)
                        }
                        {/* <Label className="pokemon-type-info pokemon-type-info-normal">Normal</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-grass">Grass</Label> */}
                        </Col>
                        {/* <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-fire">fire</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-poison">poison</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-ice">ice</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-fighting">fighting</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-water">water</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-eletric">eletric</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-ground">ground</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-flying">flying</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-psychic">psychic</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-bug">bug</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-rock">rock</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-ghost">ghost</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-dragon">dragon</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-dark">dark</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-steel">steel</Label>
                        <a> </a>
                        <Label className="pokemon-type-info pokemon-type-info-fairy">fairy</Label> */}
                    </Row>
                    <Row>
                        <Col className="lg-2">
                            <Label className="card-short-attributes"> {this.props.pokemonObj.height} | {this.props.pokemonObj.weight} | {this.props.pokemonObj.base_experience}</Label>
                        </Col>
                    </Row>
                    {/* </div> */}
                </CardBody>
                <Popover placement="right" isOpen={this.state.popoverOpen} target={'Popover-' + this.props.id} toggle={this.toggle}>
                    <PopoverHeader>{this.props.pokemonObj.pokemon_id + " - " + this.props.pokemonObj.name}</PopoverHeader>
                    <PopoverBody>Isso é apenas um teste meu consagrado!!!.</PopoverBody>
                </Popover>
            </div>
        );
    }
}

export default PokemonCard;